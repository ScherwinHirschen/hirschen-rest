<?php

namespace Hirschen\Rest\Service\ParamConverter;

use Hirschen\Rest\Constants\GroupConstants;
use Hirschen\Rest\Entity\Interfaces\EntityInterface;
use Hirschen\Rest\Entity\Interfaces\FileHolderInterface;
use Hirschen\Rest\Entity\Interfaces\DocumentInterface;
use Hirschen\Rest\Entity\Interfaces\ImportHolderInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Request\RequestBodyParamConverter;
use Hirschen\Rest\Service\ParamConverter\SerializerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JsonParamConverter
 * @package Hirschen\Rest\ParamConverter
 */
class JsonParamConverter extends RequestBodyParamConverter
{

    private $context= [];
    private $configuration;
    /**
     * @var SerializerService
     */
    protected $serializerService;

    /**
     * The name of the argument on which the ConstraintViolationList will be set.
     *
     * @var null|string
     */
    private $validationErrorsArgument;

    /**
     * JsonParamConverter constructor.
     * @param SerializerService $serializerService
     * @param null $groups
     * @param null $version
     */
    public function __construct(
        SerializerService $serializerService,
        $groups = null,
        $version = null
    ) {
        $this->serializerService=$serializerService;
        if (!empty($groups)) {
            $this->context['groups'] = (array) $groups;
        }
        if (!empty($version)) {
            $this->context['version'] = $version;
        }

        parent::__construct($serializerService->serializer,$groups,$version,$serializerService->validator,$serializerService->validationErrorsArgument);
    }

    /**
     * @param ParamConverter $configuration
     * @return bool
     */
    public function supports(ParamConverter $configuration)
    {
        return null !== $configuration->getClass() && 'json.param_converter' === $configuration->getConverter();
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     * @return bool
     * @throws \Hirschen\Rest\Exception\AccessDeniedException
     * @throws \Hirschen\Rest\Exception\DocumentNotFoundException
     * @throws \Hirschen\Rest\Exception\EntityNotFoundException
     * @throws \Hirschen\Rest\Exception\InvalidParamsException
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $options = (array) $configuration->getOptions();
        $this->configuration=$configuration;

        if (isset($options['deserializationContext']) && is_array($options['deserializationContext'])) {
            $arrayContext = array_merge($this->context, $options['deserializationContext']);
        } else {
            $arrayContext = $this->context;
        }

        $this->configureContext($context = new Context(), $arrayContext);


        $body=$this->getBody($request,$arrayContext);
        $object=$this->serializerService->deserialize($body,$configuration,$context);
        $file = $this->getFile($request);
        $import = $this->getImport($request);

        if($file instanceof UploadedFile && $object instanceof FileHolderInterface) $object->setFile($file);
        if($import instanceof UploadedFile && $object instanceof ImportHolderInterface) $object->setImport($import);

        $this->serializerService->setValidationGroups($options);
        $this->serializerService->validate($object,$options);

        if($object instanceof EntityInterface){
            $object = $this->serializerService->synchronizeEntity($object);
        } else if ($object instanceof DocumentInterface){
            $object = $this->serializerService->synchronizeDocument($object);
        }

        $request->attributes->set($configuration->getName(),$object);

        return true;
    }


    private function getFile(Request $request){
        return @$request->files->all()["file"];
    }


    private function getImport(Request $request){
        return @$request->files->all()["import"];
    }


    private function buildDefaultBody(Request $request,$isGet=true){

        $defBody = [];
        if(!$isGet){
            $defBody = $request->request->all();
            foreach ($defBody as $index => $value){

                $decoded = json_decode($value,true);
                if(is_array($decoded)) $defBody[$index] = $decoded;
            }
        } else {
            $defBody = $request->query->all();
        }

        return json_encode($defBody);
    }

    /**
     * @param Request $request
     * @param $arrayContext
     * @return mixed|resource|string
     */
    private function getBody(Request $request,$arrayContext){
        $groups=@$arrayContext["groups"];
        $fileUploaded = is_array($groups) && array_search(GroupConstants::UPLOAD,$groups)!==false;
        $isGet = $request->getMethod() === "GET";
        if($isGet || $fileUploaded){
            $body = $this->buildDefaultBody($request,$isGet);
        }
        else $body=$request->getContent();
        return $body;
    }

}