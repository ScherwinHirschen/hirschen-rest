<?php
namespace Hirschen\Rest\Service\User;

use Hirschen\Rest\Auth\RefreshTokenHandler;
use Hirschen\Rest\Constants\UserRoles;
use Hirschen\Rest\Entity\User;
use Hirschen\Rest\Exception\UserAlreadyExistsException;
use FOS\UserBundle\Model\UserManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserService
{
    /** @var  UserManagerInterface $userManager */
    private $userManager;

    /** @var  JWTTokenManagerInterface $jwtManager */
    private $jwtManager;

    /** @var  RefreshTokenHandler $refreshTokenHandler */
    private $refreshTokenHandler;

    private $tokenStorage;

    /**
     * UserService constructor.
     * @param UserManagerInterface $userManager
     * @param JWTTokenManagerInterface $jwtManager
     * @param RefreshTokenHandler $refreshTokenHandler
     */
    public function __construct(UserManagerInterface $userManager, JWTTokenManagerInterface $jwtManager, RefreshTokenHandler $refreshTokenHandler,TokenStorageInterface $tokenStorage)
    {
        $this->userManager = $userManager;
        $this->jwtManager = $jwtManager;
        $this->refreshTokenHandler = $refreshTokenHandler;
        $this->tokenStorage=$tokenStorage;
    }

    /**
     * @param User $user
     * @param bool $buildToken
     * @return User
     * @throws UserAlreadyExistsException
     */
    public function createUser(User $user, $buildToken = true): User
    {
        $existingUser = $this->userManager->findUserByEmail($user->getEmail());
        if ($existingUser) {
            throw new UserAlreadyExistsException();
        }

        $user->setEnabled(true);
        $user->setPlainPassword($user->getPassword());
        $this->updateUser($user,$buildToken);

        return $user;
    }

    /**
     * @param User $user
     * @param bool $buildToken
     * @throws \Exception
     */
    public function updateUser(User $user, $buildToken = true){
        $this->userManager->updateUser($user);
        if($buildToken){
            $token = $this->jwtManager->create($user);
            $refreshToken = $this->refreshTokenHandler->getToken($user);
            $user->setJwtToken($token);
            $user->setRefreshToken($refreshToken);
        }
    }

    /**
     * Deletes the user's refresh token (and let JWT expire)
     * @param User $user
     * @return bool
     */
    public function logoutUser(User $user): bool
    {
        $this->refreshTokenHandler->deleteToken($user->getRefreshToken());
        return true;
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     *
     * @final since version 3.4
     */
    public function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }
}
