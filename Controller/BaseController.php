<?php
namespace Hirschen\Rest\Controller;

use Hirschen\Rest\Constants\GroupConstants;
use Hirschen\Rest\Entity\Interfaces\EntityInterface;
use Hirschen\Rest\Exception\EntityNotFoundException;
use Hirschen\Rest\Repository\Pagination;
use Hirschen\Rest\Service\ParamConverter\ParameterBuilder;
use Hirschen\Rest\Service\Response\ControllerResponseFactory;
use Hirschen\Rest\Service\Response\ResponseFactory;
use Hirschen\Rest\Service\User\UserService;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * Class BaseController
 * @package Hirschen\Rest\Controller
 */
abstract class BaseController extends FOSRestController
{

    /**
     * @param EntityInterface $entity
     */
    protected function save($entity){
        $manager=$this->getManager();
        $manager->persist($entity);
        $manager->flush();
    }

    /**
     * @param EntityInterface $entity
     */
    protected function delete(EntityInterface $entity){
        $this->getManager()->remove($entity);
        $this->getManager()->flush();
    }

    /**
     * @param mixed $data
     * @param array $serialization
     * @param string $message
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function sendResponseWithData($data, string $message,array $serialization=null)
    {
        if($serialization===null) $serialization=[GroupConstants::ENTITY_OUT];

        $pages = Pagination::getPages();
        if(is_numeric($pages)) $data["total_pages"]=$pages;

        $response = $this->getControllerResponseFactory()->createWithData($data, $serialization, $message);

        return $this->getResponseFactory()->createFromControllerResponse($response);
    }

    /**
     * @return ResponseFactory
     */
    protected function getResponseFactory(): ResponseFactory
    {
        return $this->get(ResponseFactory::class);
    }

    /**
     * @return ControllerResponseFactory
     */
    protected function getControllerResponseFactory(): ControllerResponseFactory
    {
        return $this->get(ControllerResponseFactory::class);
    }



    /**
     * @return UserService
     */
    protected function getUserService(){
        return $this->get(UserService::class);

    }


    protected function getParameterBuilder(){
        return $this->get(ParameterBuilder::class);
    }

    /**
     * @param $class
     * @param $id
     * @throws EntityNotFoundException
     */
    protected function throwEntityNotFound($class,$id){
        $location = new $class();
        $location->setId($id);
        throw new EntityNotFoundException($location);
    }
}
