<?php

namespace Hirschen\Rest\Controller\Traits;


use Hirschen\Rest\Entity\Interfaces\FileHolderInterface;
use Hirschen\Rest\Entity\Interfaces\ImageInterface;
use Hirschen\Rest\Entity\Traits\FileHolderTrait;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Constraints as Assert;

trait S3Trait
{

    protected function uploadS3(FileHolderInterface $fileHolder){

        $awsBucket = $_ENV["AWS_BUCKET"];
        $this->getS3()
            ->upload(
                $awsBucket,
                $fileHolder->getPath(),
                file_get_contents($fileHolder->getFile()),
                "public-read");

    }

    protected function deleteS3(FileHolderInterface $fileHolder){
        $this->getS3()->deleteObject([
            "Bucket" => $this->getBucketName(),
            "Key" => $fileHolder->getPath()
        ]);
    }

    /**
     * @return mixed
     */
    protected function getBucketName(){
        return $_ENV["AWS_BUCKET"];
    }

    /**
     * @return mixed
     */
    protected function getS3(){
        return $this->get("aws.s3");
    }
}