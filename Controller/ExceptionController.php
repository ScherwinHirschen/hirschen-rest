<?php
namespace Hirschen\Rest\Controller;

/**
 * Class ExceptionController
 * @package Hirschen\Rest\Controller
 */
class ExceptionController extends BaseController
{
    /**
     * @param \Exception $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(\Exception $exception)
    {
        $response = $this->getControllerResponseFactory()->createWithException($exception);
        return $this->getResponseFactory()->createFromControllerResponse($response);
    }
}
