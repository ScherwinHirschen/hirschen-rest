<?php

namespace Hirschen\Rest\Repository;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Pagination
 * @package Hirschen\Rest\Repository
 */
class Pagination
{

    private static $pagination, $pages = false;

    private $pageSize;

    /**
     * @var int
     */
    private $maxLimit = 100;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    private $offset = 0;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    private $limit = 50;


    /**
     * @var int
     * @Serializer\Type("int")
     */
    private $page;

    /**
     * @var
     */
    private $sort;


    /**
     * Pagination constructor.
     * @param $page
     * @param $page_size
     * @param $sort
     */
    public function __construct($page, $page_size, $sort)
    {
        $this->pageSize = $page_size;
        $this->limit = $page_size;
        $this->offset = $page * $page_size;
        $this->page = $page;
        $this->sort = $sort;
    }

    /**
     * @param $page
     * @param $page_size
     * @param $sort
     */
    public static function buildPagination($page, $page_size, $sort)
    {
        self::$pagination = new Pagination($page, $page_size, $sort);
    }

    /**
     *
     */
    public static function unsetPagination()
    {
        self::$pagination = null;
    }

    /**
     * @return Pagination $pagination
     */
    public static function getPagination()
    {
        return self::$pagination;
    }

    /**
     * @param   $queryBuilder
     * @return mixed
     */
    public function decoratePagination($queryBuilder)
    {
        $queryBuilder = $queryBuilder->setMaxResults($this->getLimit())->setFirstResult($this->getOffset());

        if ($this->sort) {
            $queryBuilder = $queryBuilder->sort($this->sort);
        }

        return $queryBuilder;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        if ($this->offset > $this->maxLimit) $this->offset = $this->maxLimit;
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @param $maxLimit
     */
    public function setMaxLimit($maxLimit)
    {
        $this->maxLimit = $maxLimit;
    }

    /**
     * @return mixed
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @param mixed $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }


    /**
     * @return int
     */
    public static function getPages()
    {
        return self::$pages;
    }

    /**
     * @param int $pages
     */
    public static function setPages(int $pages)
    {
        self::$pages = $pages;
    }

    /**
     * @return mixed
     */
    public static function disablePagination()
    {
        $pagination = self::getPagination();
        $pageSize = $pagination->getPageSize();
        self::unsetPagination();
        return $pageSize;
    }

    /**
     * @param $count
     * @param $pageSize
     * @return float|int
     */
    public static function calcTotalPages($count, $pageSize)
    {
        $division = $count / $pageSize;
        $floor = floor($division);
        if ($division > $floor) {
            $floor += 1;
        }
        return $floor;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort): void
    {
        $this->sort = $sort;
    }


}