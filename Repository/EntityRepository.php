<?php
namespace Hirschen\Rest\Repository;


use Hirschen\Rest\ORM\QueryBuilder;
use Hirschen\Rest\Repository\Pagination;
use Doctrine\ORM\Query\Expr;

class EntityRepository extends \Doctrine\ORM\EntityRepository
{
    public function createQueryBuilder($alias, $indexBy = null)
    {
        /**
         * @var QueryBuilder $queryBuidler
         */
        $queryBuidler = (new QueryBuilder($this->_em))
            ->select($alias)
            ->from($this->_entityName, $alias, $indexBy);
        $pagination = Pagination::getPagination();
        if($pagination instanceof Pagination){
            $queryBuidler = $queryBuidler->setPagination($pagination);
        }
        return $queryBuidler;
    }
}