<?php

namespace Hirschen\Rest\EventListener;

use Hirschen\Rest\Controller\BaseController;
use Hirschen\Rest\Repository\Pagination;
use Doctrine\Common\Annotations\Reader;
use ReflectionMethod;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class PaginationListener
{
    private $annotationReader;

    public function __construct(Reader $annotationReader)
    {
        $this->annotationReader = $annotationReader;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controllerAndAction = $event->getController();

        /*
         * $controller passed can be either a class or a Closure. This is not usual in Symfony2 but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controllerAndAction)) {
            return;
        }
        $controller = $controllerAndAction[0];
        $action = $controllerAndAction[1];
        $reflectionMethod = new ReflectionMethod(get_class($controller), $action);
        $annotation = $this->annotationReader->getMethodAnnotation($reflectionMethod, \Hirschen\Rest\Annotation\Pagination::class);
        if ($controller instanceof BaseController && $annotation && $annotation instanceof \Hirschen\Rest\Annotation\Pagination) {

            $page = $event->getRequest()->get('page', 0);
            $order = $event->getRequest()->get('order', 'desc');
            $orderBy = $event->getRequest()->get('orderBy', false);
            $page_size = $annotation->getPageSize();
            $sort = false;

            if ($orderBy) {
                $sort = [];
                foreach (explode(',',$orderBy) as $field) {
                    $sort[$field] = $order;
                }
            }


            if (is_numeric($page) && is_numeric($page_size)) {
                Pagination::buildPagination($page, $page_size, $sort);
            }
        }


    }

}