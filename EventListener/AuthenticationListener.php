<?php
namespace Hirschen\Rest\EventListener;

use Hirschen\Rest\Exception\JWT\InvalidLoginException;
use Hirschen\Rest\Exception\JWT\JWTInvalidException;
use Hirschen\Rest\Exception\JWT\JWTExpiredException;
use Hirschen\Rest\Exception\JWT\JWTNotFoundException;

/**
 * Class AuthenticationListener
 * @package Hirschen\Rest\EventListener
 */
class AuthenticationListener
{
    /**
     * @throws InvalidLoginException
     */
    public function onAuthenticationFailureResponse()
    {
        throw new InvalidLoginException();
    }

    /**
     * @throws JWTInvalidException
     */
    public function onJWTInvalid()
    {
        throw new JWTInvalidException();
    }

    /**
     * @throws JWTNotFoundException
     */
    public function onJWTNotFound()
    {
        throw new JWTNotFoundException();
    }

    /**
     * @throws JWTExpiredException
     */
    public function onJWTExpired()
    {
        throw new JWTExpiredException();
    }
}
