<?php
namespace Hirschen\Rest\Auth;

use Hirschen\Rest\Entity\User;
use Gesdinet\JWTRefreshTokenBundle\Doctrine\RefreshTokenManager;
use Gesdinet\JWTRefreshTokenBundle\EventListener\AttachRefreshTokenOnSuccessListener;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Exception;
use DateTime;
use \Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenInterface;
/**
 * Class RefreshTokenHandler
 * @package Hirschen\Rest\Service\User
 */
class RefreshTokenHandler
{
    /** @var  AttachRefreshTokenOnSuccessListener $attachRefreshTokenListener  */
    private $attachRefreshTokenListener;

    /** @var  RefreshTokenManager $refreshTokenManager */
    private $refreshTokenManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var string
     */
    private $ttl;

    /**
     * @var string
     */
    private $userIdentityField;

    /**
     * RefreshTokenHandler constructor.
     * @param AttachRefreshTokenOnSuccessListener $attachRefreshTokenListener
     * @param RefreshTokenManager $refreshTokenManager
     * @param $ttl
     * @param ValidatorInterface $validator
     * @param $userIdentityField
     */
    public function __construct(
        AttachRefreshTokenOnSuccessListener $attachRefreshTokenListener,
        RefreshTokenManager $refreshTokenManager,
        $ttl,
        ValidatorInterface $validator,
        $userIdentityField)
    {
        $this->attachRefreshTokenListener = $attachRefreshTokenListener;
        $this->refreshTokenManager = $refreshTokenManager;
        $this->ttl = $ttl;
        $this->userIdentityField = $userIdentityField;
        $this->validator = $validator;
    }

    /**
     * @param User $user
     * @return string
     * @throws Exception
     */
    public function getToken(User $user): string
    {
        $refreshToken = $this->refreshTokenManager->getLastFromUsername($user->getUsername());
        if($refreshToken && !$refreshToken->isValid()) {
            $this->refreshTokenManager->revokeAllInvalid(null,true);
        }

        $refreshToken = $this->refreshTokenManager->getLastFromUsername($user->getUsername());

        if(!$refreshToken){
            $refreshToken = $this->createToken($user);
        }
        return $refreshToken->getRefreshToken();
    }

    /**
     * @param User $user
     * @return RefreshTokenInterface
     * @throws Exception
     */
    public function createToken(User $user){

        $datetime = new DateTime();
        $datetime->modify('+'.$this->ttl.' seconds');

        $refreshToken = $this->refreshTokenManager->create();

        $accessor = new PropertyAccessor();
        $userIdentityFieldValue = $accessor->getValue($user, $this->userIdentityField);

        $refreshToken->setUsername($userIdentityFieldValue);
        $refreshToken->setRefreshToken();
        $refreshToken->setValid($datetime);

        $valid = false;
        while (false === $valid) {
            $valid = true;
            $errors = $this->validator->validate($refreshToken);
            if ($errors->count() > 0) {
                foreach ($errors as $error) {
                    if ('refreshToken' === $error->getPropertyPath()) {
                        $valid = false;
                        $refreshToken->setRefreshToken();
                    }
                }
            }
        }

        $this->refreshTokenManager->save($refreshToken);
        return $refreshToken;
    }


    /**
     * @param string $token
     */
    public function deleteToken(string $token)
    {
        $token = $this->refreshTokenManager->get($token);
        if ($token) {
            $this->refreshTokenManager->delete($token, true);
        }
    }
}
