<?php
namespace Hirschen\Rest\Exception\JWT;
use Hirschen\Rest\Exception\Base\ApiException;

/**
 * Class JWTExpiredException
 * @package Hirschen\Rest\Exception\JWT
 */
class JWTExpiredException extends ApiException
{
    const MESSAGE = 'JWT expired!';
    const STATUS_CODE = 401;

    /**
     * JWTExpiredException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [],self::MESSAGE);
    }
}
