<?php
namespace Hirschen\Rest\Exception\JWT;
use Hirschen\Rest\Exception\Base\ApiException;

/**
 * Class JWTInvalidException
 * @package Hirschen\Rest\Exception\JWT
 */
class JWTInvalidException extends ApiException
{

    const MESSAGE = 'Invalid JWT Token';
    const STATUS_CODE = 401;

    /**
     * InvalidJWTException constructor.
     */
    public function __construct() {
        parent::__construct(self::STATUS_CODE, [],self::MESSAGE);
    }
}
