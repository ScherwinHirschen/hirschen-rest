<?php
namespace Hirschen\Rest\Exception\Interfaces;

/**
 * Interface ApiExceptionInterface
 * @package Hirschen\Rest\Exception\Interfaces
 */
interface ApiExceptionInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return null|array
     */
    public function getErrorData(): ?array;
}
