<?php

namespace Hirschen\Rest\Exception;


use Hirschen\Rest\Exception\Base\ApiException;

/**
 * Class AccessDeniedException
 * @package Hirschen\Rest\Exception
 */
class AccessDeniedException extends ApiException
{
    /**
     * AccessDeniedException constructor.
     * @param array $errorData
     */
    public function __construct(array $errorData=[])
    {
        parent::__construct(403, $errorData, "Access Denied for User");
    }

}