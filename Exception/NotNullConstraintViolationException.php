<?php

namespace Hirschen\Rest\Exception;
use Hirschen\Rest\Exception\Interfaces\ApiExceptionInterface;

/**
 * Class NotNullConstraintViolationException
 * @package Hirschen\Rest\Exception
 */
class NotNullConstraintViolationException extends \Doctrine\DBAL\Exception\NotNullConstraintViolationException implements ApiExceptionInterface
{
    /**
     * NotNullConstraintViolationException constructor.
     * @param \Doctrine\DBAL\Exception\NotNullConstraintViolationException $exception
     */
    public function __construct(\Doctrine\DBAL\Exception\NotNullConstraintViolationException $exception)
    {
        parent::__construct($exception->getMessage(), $exception->getPrevious());
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return 409;
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return [];
    }
}