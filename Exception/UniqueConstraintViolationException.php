<?php


namespace Hirschen\Rest\Exception;
use Hirschen\Rest\Exception\Interfaces\ApiExceptionInterface;

/**
 * Class UniqueConstraintViolationException
 * @package Hirschen\Rest\Exception
 */
class UniqueConstraintViolationException extends \Doctrine\DBAL\Exception\UniqueConstraintViolationException implements ApiExceptionInterface
{
    /**
     * UniqueConstraintViolationException constructor.
     * @param \Doctrine\DBAL\Exception\UniqueConstraintViolationException $exception
     */
    public function __construct( \Doctrine\DBAL\Exception\UniqueConstraintViolationException $exception)
    {
        parent::__construct($exception->getMessage(), $exception->getPrevious());
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return 409;
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return [];
    }

}