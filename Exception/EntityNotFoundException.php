<?php

namespace Hirschen\Rest\Exception;


use Hirschen\Rest\Entity\Interfaces\EntityInterface;
use Hirschen\Rest\Exception\Interfaces\ApiExceptionInterface;

/**
 * Class EntityNotFoundException
 * @package Hirschen\Rest\Exception
 */
class EntityNotFoundException extends \Doctrine\ORM\EntityNotFoundException implements ApiExceptionInterface
{
    /**
     * EntityNotFoundException constructor.
     * @param EntityInterface $entity
     */
    public function __construct(EntityInterface $entity)
    {
        parent::__construct(
            'Entity of type \'' . get_class($entity) . '\'' . ($entity->getId() ? ' for ID ' . $entity->getId() : '') . ' was not found',
            404);
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->code;
    }

    /**
     * @return array|null
     */
    public function getErrorData() : ?array
    {
        return ["Entity not found"];
    }
}