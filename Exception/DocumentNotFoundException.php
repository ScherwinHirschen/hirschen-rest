<?php

namespace Hirschen\Rest\Exception;


use Hirschen\Rest\Entity\Interfaces\EntityInterface;
use Hirschen\Rest\Exception\Interfaces\ApiExceptionInterface;
use Hirschen\Rest\Entity\Interfaces\DocumentInterface;
use Exception;

/**
 * Class EntityNotFoundException
 * @package Hirschen\Rest\Exception
 */
class DocumentNotFoundException extends Exception implements ApiExceptionInterface
{
    /**
     * DocumentNotFoundException constructor.
     * @param DocumentInterface $document
     */
    public function __construct(DocumentInterface $document)
    {
        parent::__construct(
            'Document of type \'' . get_class($document) . '\'' . ($document->getId() ? ' for ID ' . $document->getId() : '') . ' was not found',
            404);
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return 404;
    }

    /**
     * @return array|null
     */
    public function getErrorData() : ?array
    {
        return ["Document not found"];
    }
}