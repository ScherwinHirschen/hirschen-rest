<?php
namespace Hirschen\Rest\Entity;


use Hirschen\Rest\Entity\Interfaces\EntityInterface;
use Hirschen\Rest\Entity\Traits\IdTrait;
use Hirschen\Rest\Entity\Traits\SyncAbleTrait;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use Hirschen\Rest\Constants\GroupConstants as G;

class User extends BaseUser
{
    const ROLE_USER="ROLE_USER";
    const ROLE_ADMIN="ROLE_ADMIN";
    const PARAM_CONV ="USER_ID";

    /**
     * @var string $jwtToken
     * @Serializer\Type("string")
     * @Groups({G::ENTITY_OUT})
     */
    private $jwtToken;

    /**
     * @var string $refreshToken
     * @Assert\NotBlank(groups={G::LOGOUT})
     * @Serializer\Type("string")
     * @Groups({G::ENTITY_OUT, G::LOGOUT})
     */
    private $refreshToken;


    public function __construct()
    {
        parent::__construct();
        $this->roles = [];
    }

    /**
     * @return string
     */
    public function getJwtToken()
    {
        return $this->jwtToken;
    }

    /**
     * @param string $jwtToken
     */
    public function setJwtToken(string $jwtToken)
    {
        $this->jwtToken = $jwtToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken(string $refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    public function getRole(){
        $res = self::ROLE_USER;
        $roles = array_filter($this->getRoles(),function($role){
            return $role !== self::ROLE_USER;
        });
        if(sizeof($roles) > 0){
            $res = $roles[0];
        }

        return $res;
    }
}
