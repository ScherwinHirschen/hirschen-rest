<?php


namespace Hirschen\Rest\Entity\Interfaces;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface FileHolderInterface
 * @package Hirschen\Rest\Entity\Interfaces
 */
interface FileHolderInterface
{
    /**
     * @return UploadedFile
     */
    public function getFile();

    /**
     * @param UploadedFile $file
     * @return void
     */
    public function setFile(UploadedFile $file): void;


}