<?php
namespace Hirschen\Rest\Entity\Interfaces;

use Hirschen\Rest\Entity\Article;
use Hirschen\Rest\Entity\Company;
use Hirschen\Rest\Entity\User;

/**
 * Interface OwnAbleInterface
 * @package Hirschen\Rest\Entity\Interfaces
 */
interface OwnAbleInterface{
    /**
     * @param User $user
     * @return bool
     */
    public function isOwnedBy(User $user);

    /**
     * @return User
     */
    public function getOwner():User;
}
