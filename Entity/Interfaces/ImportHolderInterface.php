<?php


namespace Hirschen\Rest\Entity\Interfaces;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface FileHolderInterface
 * @package Hirschen\Rest\Entity\Interfaces
 */
interface ImportHolderInterface
{
    /**
     * @return UploadedFile
     */
    public function getImport();

    /**
     * @param UploadedFile $import
     * @return void
     */
    public function setImport(UploadedFile $import): void;


}