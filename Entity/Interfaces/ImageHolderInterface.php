<?php


namespace Hirschen\Rest\Entity\Interfaces;


/**
 * Interface FileHolderInterface
 * @package Hirschen\Rest\Entity\Interfaces
 */
interface ImageHolderInterface
{

    /**
     * @return string
     */
    public function getPath();

    /**
     * @param $image
     */
    public function setImage($image): void;

    /**
     * @param string
     */
    public function getImage();


}