<?php
namespace Hirschen\Rest\Entity\Interfaces;

/**
 * Interface ValidateAbleInterface
 * @package Hirschen\Rest\Entity\Interfaces
 */
interface ValidateAbleInterface{

    /**
     * @return void
     */
    public function validateInsert() : void;

    /**
     * @return void
     */
    public function validateUpdate() : void;
}