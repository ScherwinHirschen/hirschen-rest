<?php


namespace Hirschen\Rest\Entity\Interfaces;


/**
 * Interface DocumentInterface
 * @package Hirschen\Rest\Entity\Interfaces
 */
interface DocumentInterface
{

    /**
     * @return array
     */
    public function getId();

    /**
     * @param DocumentInterface $newDocument
     * @param DocumentInterface|null $defaultDocument
     * @return mixed
     */
    public function sync(DocumentInterface $newDocument,DocumentInterface $defaultDocument=null);

    /**
     * @param DocumentInterface $document
     * @return mixed
     */
    public function getChangedKeys(DocumentInterface $document);

}