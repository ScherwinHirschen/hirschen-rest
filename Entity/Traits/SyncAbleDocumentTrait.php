<?php

namespace Hirschen\Rest\Entity\Traits;

use Hirschen\Rest\Entity\Interfaces\DocumentInterface;


trait SyncAbleDocumentTrait
{

    /**
     * @return array
     */
    protected static function getAttributeKeys()
    {
        return array_keys(get_class_vars(__CLASS__));
    }

    /**
     * @param $key
     * @return string
     */
    public function _getter($key)
    {

        return "get" . ucfirst($key);
    }

    /**
     * @param $key
     * @return string
     */
    public function _setter($key)
    {
        return "set" . ucfirst($key);
    }

    /**
     * @return bool
     */
    public function isInstanceOfThis()
    {
        $result = true;
        $args = func_get_args();
        if (sizeof($args) < 1) return false;
        foreach ($args as $entity) {
            if (get_class($this) !== get_class($entity)) $result = false;
        }
        return $result;
    }

    protected function keyIsSkippable($key): bool
    {
        return $key === 'createdAt';
    }

    /**
     * @param DocumentInterface $defaultEntity
     * @return array
     */
    public function getChangedKeys(DocumentInterface $defaultEntity)
    {
        $result = [];
        if ($this->isInstanceOfThis($defaultEntity)) {
            foreach (self::getAttributeKeys() as $key) {
                if ($this->keyIsSkippable($key)) continue;
                $getter = $this->_getter($key);
                if (method_exists($this, $getter) && $key !== "id" && $defaultEntity->$getter() !== $this->$getter()) $result[] = $key;
            }
        }
        return $result;
    }


    /**
     * @param DocumentInterface $newDocument
     * @param DocumentInterface|null $defaultDocument
     */
    public function sync(DocumentInterface $newDocument, DocumentInterface $defaultDocument = null)
    {

        if ($defaultDocument === null) {
            $class = get_class($defaultDocument);
            $defaultDocument = new $class();
        }

        if ($this->isInstanceOfThis($newDocument, $defaultDocument)) {
            $changedKeys = $newDocument->getChangedKeys($defaultDocument);

            foreach ($changedKeys as $key) {

                $setter = $this->_setter($key);
                $getter = $this->_getter($key);
                if (
                    method_exists($this, $setter) &&
                    method_exists($newDocument, $getter) &&
                    $this->$getter() !== $newDocument->$getter()
                ) {
                    $value = $newDocument->$getter();

                    $this->$setter($value);
                }

            }

        }
    }
}