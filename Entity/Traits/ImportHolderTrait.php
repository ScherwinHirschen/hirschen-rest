<?php

namespace Hirschen\Rest\Entity\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Hirschen\Rest\Constants\GroupConstants;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;



trait ImportHolderTrait
{
    /**
     * @Assert\NotBlank(groups={GroupConstants::FORCE_IMPORT})
     * @var UploadedFile
     */
    protected $import;

    /**
     * @return UploadedFile
     */
    public function getImport()
    {
        return $this->import;
    }

    /**
     * @param UploadedFile $import
     */
    public function setImport(UploadedFile $import): void
    {
        $this->import = $import;
    }






}