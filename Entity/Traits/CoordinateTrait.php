<?php
namespace Hirschen\Rest\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Hirschen\Rest\Annotation\NotBlank;
use Hirschen\Rest\Annotation\CustomGroups;
use JMS\Serializer\Annotation\Groups;

/**
 * Class CoordinateTrait
 * @package Hirschen\Rest\Entity\Traits
 */
trait CoordinateTrait{

    /**
     * @Assert\NotBlank(groups={G::NEW})
     * @Groups({G::EDIT,G::NEW,G::ENTITY_OUT})
     * @ORM\Column(type="float")
     * @var $lat float
     */
    protected $lat;

    /**
     * @Assert\NotBlank(groups={G::NEW})
     * @Groups({G::EDIT,G::NEW,G::ENTITY_OUT})
     * @ORM\Column(type="float")
     * @var $lng float
     */
    protected $lng;

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param mixed $lng
     */
    public function setLng($lng): void
    {
        $this->lng = $lng;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat): void
    {
        $this->lat = $lat;
    }

}