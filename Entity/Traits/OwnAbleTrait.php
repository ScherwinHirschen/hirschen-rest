<?php

namespace Hirschen\Rest\Entity\Traits;


use Hirschen\Rest\Entity\Article;
use Hirschen\Rest\Entity\ArticleImage;
use Hirschen\Rest\Entity\ArticleSize;
use Hirschen\Rest\Entity\Interfaces\EntityInterface;
use Hirschen\Rest\Entity\Interfaces\OwnAbleInterface;
use Hirschen\Rest\Entity\User;

trait OwnAbleTrait
{
    /**
     * @param User $user
     * @return bool
     */
    public function isOwnedBy(User $user){

        if($user->isAdmin())return true;

        $user=$this->getOwner();
        $res=$user->getId() === $user->getId();

        return $res;
    }

}