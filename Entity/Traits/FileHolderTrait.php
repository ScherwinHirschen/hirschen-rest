<?php

namespace Hirschen\Rest\Entity\Traits;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Hirschen\Rest\Constants\GroupConstants;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Groups;



trait FileHolderTrait
{
    /**
     * @Assert\NotBlank(groups={GroupConstants::FORCE_UPLOAD})
     * @var UploadedFile
     */
    protected $file;


    /**
     * @return UploadedFile
     */
    public function getFile(){
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file): void
    {
        $this->file=$file;
    }



}