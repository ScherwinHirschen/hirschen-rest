<?php
namespace Hirschen\Rest\Annotation;
use JMS\Serializer\Annotation\Groups;
use Hirschen\Rest\Constants\GroupConstants as G;
use Hirschen\Rest\Annotation\Traits\GroupFetcherTrait;

/**
 * Class Parameter
 * @package Hirschen\Rest\Controller\Annotation
 * @Annotation
 */
class CustomGroups extends Groups
{
    use GroupFetcherTrait;

    /**
     * CustomGroups constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {

        $values =[G::ENTITY_OUT,G::INDEX_OUT];
        $idGroup = $this->fetchGroup();
        if($idGroup){
            $values[]=$idGroup;
        }
        $this->groups = $values;
    }
}
