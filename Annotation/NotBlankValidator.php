<?php
namespace Hirschen\Rest\Annotation;
use Hirschen\Rest\Annotation\Traits\GroupFetcherTrait;
use Doctrine\Common\Annotations\DocParser;
use JMS\Serializer\Annotation\Groups;
use Hirschen\Rest\Constants\GroupConstants;

/**
 * Class Parameter
 * @package Hirschen\Rest\Annotation
 */
class NotBlankValidator extends \Symfony\Component\Validator\Constraints\NotBlankValidator
{

}
